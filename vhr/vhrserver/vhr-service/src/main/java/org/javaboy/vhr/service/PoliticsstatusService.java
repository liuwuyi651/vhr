package org.javaboy.vhr.service;

import org.javaboy.vhr.mapper.PoliticsstatusMapper;
import org.javaboy.vhr.model.Politicsstatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created By WuYi.
 */
@Service
public class PoliticsstatusService {

    private final PoliticsstatusMapper politicsstatusMapper;

    @Autowired
    public PoliticsstatusService(PoliticsstatusMapper politicsstatusMapper) {
        this.politicsstatusMapper = politicsstatusMapper;
    }

    public List<Politicsstatus> getAllPoliticsstatus()
    {
        return politicsstatusMapper.getAllPoliticsstatus();
    }
}
